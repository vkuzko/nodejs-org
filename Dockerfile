FROM node:16
# Creatind workdir
ARG APP_DIR=app
RUN mkdir -p ${APP_DIR}
WORKDIR ${APP_DIR}
# Copying project files
COPY . .
# Expose port
EXPOSE 8080
# Project run
CMD ["npm", "start"]